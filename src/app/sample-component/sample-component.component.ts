import { ModalService } from "./../modal-service.service";
import { Component } from "@angular/core";

@Component({
  selector: "app-sample",
  templateUrl: "./sample-component.component.html",
  styleUrls: ["./sample-component.component.scss"]
})
export class SampleComponent {
  constructor(private modalService: ModalService) {}

  stepOne: boolean = true;
  stepTwo: boolean = false;
  stepThree: boolean = false;

  public continue() {
    this.stepOne = false;
    this.stepTwo = true;
  }

  public close() {
    this.modalService.destroy();
  }
}
