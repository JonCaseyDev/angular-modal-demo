import { Component } from "@angular/core";
import { ModalService } from "./modal-service.service";
import { SampleComponent } from "./sample-component/sample-component.component";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "modal window";

  constructor(private modalService: ModalService) {}

  initLoginModal() {
    let inputs = {
      isMobile: false
    };

    this.modalService.show(SampleComponent, inputs, {});
  }
}
