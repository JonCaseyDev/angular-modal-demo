import { ModalService } from "./modal-service.service";
import { DomService } from "./dom-service.service";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { SampleComponent } from "./sample-component/sample-component.component";

@NgModule({
  declarations: [AppComponent, SampleComponent],
  imports: [BrowserModule, AppRoutingModule],
  providers: [DomService, ModalService],
  bootstrap: [AppComponent],
  entryComponents: [SampleComponent]
})
export class AppModule {}
// #https://stackblitz.com/edit/angular-uimsw4
